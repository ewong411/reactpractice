import React, { Component } from "react";
import NavBar from "./components/navbar";
import Counter from "./components/counters";
import "./App.css";

/*Main Componenet*/

class App extends Component {
  state = {
    counters: [
      { id: 1, value: 0 },
      { id: 2, value: 0 },
      { id: 3, value: 0 },
      { id: 4, value: 0 }
    ]
  };
  /**
   * used to initialize properties
   */
  constructor() {
    //if using props must pass props into the arguments if not its undefined
    super();
    console.log("App-Constructor");
  }
  /**
   * used for ajax calls
   */
  componentDidMount() {
    console.log("App-Mounted");
  }
  handleDelete = counterID => {
    console.log("Event Handler Called", counterID);
    const counters = this.state.counters.filter(c => c.id !== counterID);
    this.setState({ counters });
  };
  handleIncrement = counter => {
    const counters = [...this.state.counters];
    const index = counters.indexOf(counter);
    counters[index] = { ...counter };
    counters[index].value++;
    this.setState({ counters });
  };
  handleDecrement = counter => {
    const counters = [...this.state.counters];
    const index = counters.indexOf(counter);
    counters[index] = { ...counter };
    counters[index].value--;
    this.setState({ counters });
  };
  handleAddItem = counter => {
    let counters = [...this.state.counters];
    counters.push({ id: counters.length + 1, value: 0 });
    this.setState({ counters });
  };
  handleReset = () => {
    this.state.counters.map(c => {
      c.value = 0;
      return c;
    });
    const counters = [...this.state.counters];
    this.setState({ counters });
  };

  render() {
    console.log("App-Rendered");
    return (
      <React.Fragment>
        <NavBar
          totalCounters={this.state.counters.filter(c => c.value > 0).length}
        />
        <main className="Container">
          <Counter
            counters={this.state.counters}
            onReset={this.handleReset}
            onIncrement={this.handleIncrement}
            onDelete={this.handleDelete}
            onDecrement={this.handleDecrement}
            onAdd={this.handleAddItem}
          />
        </main>
      </React.Fragment>
    );
  }
}

export default App;
