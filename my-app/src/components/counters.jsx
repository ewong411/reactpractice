import React, { Component } from "react";
import Counter from "./counter";

class Counters extends Component {
  render() {
    console.log("Counters - Rendered");

    //Deconstructing Arguments
    const {
      onReset,
      counters,
      onDelete,
      onIncrement,
      onDecrement,
      onZeroed,
      onAdd
    } = this.props;

    return (
      <div>
        <button onClick={onReset} className="btn btn-primary btn-sm m2">
          Reset
        </button>
        <button className="btn btn-primary btn-sm m-2" onClick={onAdd}>
          Add Item
        </button>
        {counters.map(counter => (
          <Counter
            key={counter.id}
            onDelete={onDelete}
            onIncrement={onIncrement}
            onDecrement={onDecrement}
            counter={counter}
            onAdd={onAdd}
          >
            <h4>Counter #{counter.id}</h4>
          </Counter>
        ))}
      </div>
    );
  }
}
export default Counters;
