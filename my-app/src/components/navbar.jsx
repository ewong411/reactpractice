import React, { Component } from "react";

//Stateless Functional Component
//sfc shortcut for it
const NavBar = ({ totalCounters }) => {
  console.log("NavBar - Rendered");
  //deconstructing arguments
  return (
    <nav className="navbar navbar-light bg-light">
      <a className="navbar-brand" href="#">
        NavBar
        <span className="bade badge-pill badge-secondary m-2">
          {totalCounters}
        </span>
      </a>
    </nav>
  );
};

export default NavBar;
