import React, { Component } from "react";

/**
 *
 */
class Counter extends Component {
  state = {
    tags: ["tage1", "tage2", "tage3"]
    //imageUrl: "https://picsum.photos/200"
  };
  styles = {
    fontWeight: "bold",
    fontSize: 18
  };

  //decide whether to get new data based on the prevProps and prevState objects
  componentDidUpdate(prevProps, prevState) {
    console.log("prevProps", prevProps);
    console.log("prevState", prevState);
    if (prevProps.counter.value === 0) {
      //Ajax call and get new data from the server
    }
  }
  // gives us opportunity to clean up resources eg Timers or listeners before component remove from the dom
  //there will be memory leaks if so
  componentWillUnmount() {
    console.log("Counter-Unmount");
  }

  renderTags() {
    if (this.state.tags.length === 0) {
      return <p> There are no Tags!</p>;
    }
    return (
      <ul>
        {this.state.tags.map(tag => (
          <li key={tag}>{tag}</li>
        ))}
      </ul>
    );
  }

  getBadgeClasses() {
    let classes = "badge m-2 badge-";
    classes += this.props.counter.value === 0 ? "warning" : "primary";
    return classes;
  }

  formatCount() {
    const { value: count } = this.props.counter;
    return count === 0 ? "Zero" : count;
  }
  render() {
    //deconstructing arguments
    const {
      counter,
      onIncrement,
      onDelete,
      onDecrement,
      children
    } = this.props;

    console.log("Counter - Rendered");
    return (
      <div>
        {children}
        <span className={this.getBadgeClasses()}>{this.formatCount()}</span>
        <button
          onClick={() => onIncrement(counter)}
          className="btn btn-secondary btn-sm"
        >
          <i className="fa fa-plus"></i>
        </button>
        <button
          onClick={() => onDecrement(counter)}
          className="btn btn-secondary btn-sm m-2"
          disabled={counter.value === 0}
        >
          <i className="fa fa-minus"></i>
        </button>
        <button
          onClick={() => onDelete(counter.id)}
          className="btn btn-danger btn-sm "
        >
          <i className="fa fa-close"></i>
        </button>
      </div>
    );
  }
}

// #endregion

export default Counter;
